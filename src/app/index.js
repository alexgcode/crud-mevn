import Vue from 'vue';
import App from './components/App.vue';

new Vue({
    render: h => h(App)    //opcion de configuracion "render" q tiene una funcion valor
}).$mount('#app'); //le dice que ira en un div la app de vue