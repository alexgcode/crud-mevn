/*archivo iniciador de servidor*/
/* contiene express, nodemon(evita tener q apagar y encender el servidor) */
const express = require('express');
const morgan  = require('morgan'); 
const mongoose = require('mongoose');

const app = express();
mongoose.connect('mongodb://localhost/basic-db')
    .then(db => console.log('db conectada'))
    .catch(err => console.error(err));

// Settings of Express
app.set('port', process.env.PORT || 4000); //busca un puerto definido en el servidor, y si no, usa el 4000


// Middlewares of Express (funciones q se ejecutan antes que ingrese a las rutas, lleguen urls)
app.use(morgan('dev'));
app.use(express.json());    

// Routes of Express
app.use('/api/tasks',require('./routes/tasks.js')); /* todas las rutas de tasks.js van a anteceder con /tasks*/

// Static files of Express (los archivos q se envian 1 vez al navegador: fuentes, js, html, css, img)
app.use(express.static(__dirname + '\\public'));


// Server listening
app.listen(app.get('port'),() => {
    console.log('Server on port ' + app.get('port'));
});