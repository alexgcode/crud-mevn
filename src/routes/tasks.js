const express = require('express');
const router = express.Router();

const Task = require('../models/Task');

// api task
router.get('/',async (req, res) => {  //llamada asincrona a entender
    const tasks = await Task.find(); //is like select * from of mongoose, 
    res.json(tasks);
    console.log(tasks);
});

router.get('/:id', async (req, res) => {
    const task =  await Task.findById(req.params.id);
    res.json(task);
});

router.post('/', async (req, res) => {  //agrega tarea
    const task = new Task(req.body);    //falta mensaje de error si no se enviaron los nombres de parametros correctos
    await task.save();
    console.log(task);
    res.json({
        status: 'Task saved'
    });
});

router.put('/:id', async (req, res) => {
    await Task.findByIdAndUpdate(req.params.id, req.body);
    console.log(req.params);
    res.json({
        status: "task updated"
    });
});

router.delete('/:id', async (req, res) => {
    await Task.findByIdAndRemove(req.params.id);
    console.log(req.params);
    res.json({
        status: "task deleted"
    });
});

module.exports = router;
