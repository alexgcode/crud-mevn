const { VueLoaderPlugin } = require('vue-loader');

module.exports = {  
    entry: './src/app/index.js',   // el js de desarrollo inicial q se va a traducir js a codigo de producción js compatible con todos
    output: {
        path: __dirname + '/src/public/js',    //a donde va a colocar el archivo traducido
        filename: 'bundle.js'   
    },
    module: {
        rules: [
            {                   //1era regla: buscar los archivos .js y traducirlos a codgio para cualquier navegador
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {                   //2da regla: busca archivos .vue los traduce a codigo js para cualquier navegador
                test: /\.vue$/,
                use: {
                    loader: 'vue-loader'
                }
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
}